$(document).ready(function () {
    // Registration Form Validation
    $('#registrationForm').validate({
        rules: {
            email: {
                required: true,
                email: true,
                remote: {
                    url: "main_controller/email_not_registered",
                    type: "post",
                    data: {
                        email: function () { return $("#email").val(); }
                    }
                }
            },
            name: {
                required: true
            },
            password: {
                required: true,
                maxlength: 10,
                minlength: 3
            },
            cnf_password: {
                equalTo: "#password"
            },
            mobile: {
                required: true,
                remote: {
                    url: "main_controller/register_mobile_exists",
                    type: "post",
                    data: {
                        mobile: function () { return $("#mobile").val(); }
                    }
                },
                maxlength: 10,
                minlength: 10
            }
        },
        messages: {
            email: {
                required: ' Email address is required !!',
                email: ' Please enter a valid email address !!',
                remote: ' Email id already registered kindly login !!'
            },
            name: {
                required: " Name can't be blank !!"
            },
            password: {
                required: "Password can't be blank !!",
                maxlength: "Password should be between 3 to 10 digits !!",
                minlength: "Password should be between 3 to 10 digits !!"
            },
            cnf_password: {
                equalTo: "Entered password is not matching !!"
            },
            mobile: {
                required: "Mobile no. can't be blank !!",
                remote: 'Mobile number already registered !!',
                maxlength: "Mobile no. should of 10 digits !!",
                minlength: "Mobile no. should of 10 digits !!"
            }
        }
    });
    // Login Form Validation
    $('#loginForm').validate({
        rules: {
            email: {
                required: true,
                email: true,
                remote: {
                    url: "main_controller/register_email_exists",
                    type: "post",
                    data: {
                        email: function () { return $("#email").val(); }
                    }
                }
            },
            password: {
                required: true,
                maxlength: 10,
                minlength: 3
            }
        },
        messages: {
            email: {
                required: ' Email address is required !!',
                email: ' Please enter a valid email address !!',
                remote: ' Email id not registered, kindly register !!'
            },
            password: {
                required: "Password can't be blank !!",
                maxlength: "Password should be between 3 to 10 digits !!",
                minlength: "Password should be between 3 to 10 digits !!"
            }
        }
    });
    // Forgot password email validation 
    $('#forgotPasswordForm').validate({
        rules: {
            email: {
                required: true,
                email: true,
                remote: {
                    url: "main_controller/register_email_exists",
                    type: "post",
                    data: {
                        email: function () { return $("#email").val(); }
                    }
                }
            }
        },
        messages: {
            email: {
                required: ' Email address is required !!',
                email: ' Please enter a valid email address !!',
                remote: ' Email id not registered, kindly register !!'
            }
        }
    });
    // Otp check validation
    $('#otpForm').validate({
        rules: {
            otp: {
                required: true,
                maxlength: 5,
                minlength: 5
            }
        },
        messages: {
            otp: {
                required: ' OTP is required !!',
                maxlength: "OTP should be of 5 digits !!",
                minlength: "OTP should be of 5 digits !!"                
            }
        }
    });
    // New password input after reset password through OTP
    $('#newPasswordForm').validate({
        rules: {
            password: {
                required: true,
                maxlength: 10,
                minlength: 3
            },
            cnf_password: {
                equalTo: "#password"
            }
        },
        messages: {
            password: {
                required: "Password can't be blank !!",
                maxlength: "Password should be between 3 to 10 digits !!",
                minlength: "Password should be between 3 to 10 digits !!"
            },
            cnf_password: {
                equalTo: "Entered password is not matching !!"
            }
        }
    });
});