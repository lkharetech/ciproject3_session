<?php
class Main_model extends CI_Model
{
    // Insert data (registration)
    public function insert($data = array())
    {
        if (!empty($data)) {
            $insert = $this->db->insert('users', $data);
            return $insert ? $this->db->insert_id() : false;
        }
        return false;
    }
    // Get registered data during login & after login 
    function getRows($params = array())
    {
        $this->db->select('*');
        $this->db->from('users');
        if (array_key_exists("conditions", $params)) {
            foreach ($params['conditions'] as $key => $val) {
                $this->db->where($key, $val);
            }
        }
        if (array_key_exists("id", $params) || $params['returnType'] == 'single') {
            if (!empty($params['id'])) {
                $this->db->where('id', $params['id']);
            }
            $query = $this->db->get();
            $result = $query->row_array();
            return $result;
        }
    }
    // Email already exists check
    function email_exists($email)
    {
        $this->db->where('email', $email);
        $query = $this->db->get('users');
        if ($query->num_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    // Mobile already exists check
    function mobile_exists($mobile) {
        $this->db->where('mobile', $mobile);
        $query = $this->db->get('users');
        if ($query->num_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    // Update Otp
    function updateOTP($email, $otp)
    {
        $this->db->where('email', $email);
        $update = $this->db->update('users', array('otp' => $otp));
        return $update ? true : false;
    }
    // Check for email id exist in database or not
    function getData($email)
    {
        $query = $this->db->get('users');
        $result = $query->result();
        foreach($result as $row)
        {
            if($row->email == $email) {
                return $email;
            } 
        }
        echo "Email not registered";die;
    }
    // Check for email id & otp exist in database or not
    function getDataPassword($otp, $email) {
        $query = $this->db->get('users');
        $result = $query->result_array();
        foreach($result as $row)
        {
            if($row['email'] == $email && $row['otp'] == $otp) {
                return $row;
            }
        }
        return "0";
    }
    // Update new password
    function updatePassword($email, $otp, $password) {
        $this->db->where('email', $email);
        $this->db->where('otp', $otp);
        $update = $this->db->update('users', array('password' => $password));
        return $update ? true : false;
    }
}
