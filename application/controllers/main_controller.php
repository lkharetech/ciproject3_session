<?php
class Main_controller extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('main_model');
        // User login status 
        $this->isUserLoggedIn = $this->session->userdata('isUserLoggedIn');
    }
    public function index()
    {
        if ($this->isUserLoggedIn) {
            redirect('account');
        } else {
            redirect('login');
        }
    }
    // Login function
    public function login()
    {
        if ($this->isUserLoggedIn) {
            redirect('account');
        } else {
            $data = array();
            if ($this->session->userdata('success_msg')) {
                $data['success_msg'] = $this->session->userdata('success_msg');
                $this->session->unset_userdata('success_msg');
            }
            if ($this->session->userdata('error_msg')) {
                $data['error_msg'] = $this->session->userdata('error_msg');
                $this->session->unset_userdata('error_msg');
            }
            if ($this->input->post('loginSubmit')) {
                $con = array(
                    'returnType' => 'single',
                    'conditions' => array(
                        'email' => $this->input->post('email'),
                        'password' => md5($this->input->post('password')),
                        'status' => 1
                    )
                );
                $checkLogin = $this->main_model->getRows($con);
                if ($checkLogin) {
                    $this->session->set_userdata('isUserLoggedIn', TRUE);
                    $this->session->set_userdata('userId', $checkLogin['id']);
                    redirect('account');
                } else {
                    $data['error_msg'] = 'Wrong password, please try again.';
                }
            }
            $this->load->view('view_login', $data);
        }
    }
    // Registration function
    public function registration()
    {
        if ($this->isUserLoggedIn) {
            redirect('account');
        } else {
            $data = $userData = array();

            if ($this->input->post('postSignup')) {
                $userData = array(
                    'name' => strip_tags($this->input->post('name')),
                    'email' => strip_tags($this->input->post('email')),
                    'password' => md5($this->input->post('password')),
                    'gender' => $this->input->post('gender'),
                    'mobile' => strip_tags($this->input->post('mobile')),
                );
                $insert = $this->main_model->insert($userData);
                if ($insert) {
                    $this->session->set_userdata('success_msg', 'Your account registration has been successful !! Please login to your account.');
                    redirect('login');
                } else {
                    $data['error_msg'] = 'Some problems occured while registration !!, please try again.';
                }
            }
            $data['user'] = $userData;
            $this->load->view('view_registration');
        }
    }
    // After login successfull view page
    function account()
    {
        $data = array();

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->main_model->getRows($con);
            $this->load->view('view_account', $data);
        } else {
            redirect('login');
        }
    }
    // Logout function
    function logout()
    {
        $this->session->unset_userdata('isUserLoggedIn');
        $this->session->unset_userdata('userId');
        $this->session->sess_destroy();
        redirect('login');
    }
    // Email exist or not check during login
    function register_email_exists()
    {
        if ($this->main_model->email_exists($this->input->post('email')) == TRUE) {
            echo json_encode(TRUE);
        } else {
            echo json_encode(FALSE);
        }
    }
    // Mobile already check
    function register_mobile_exists()
    {
        if ($this->main_model->mobile_exists($this->input->post('mobile')) == TRUE) {
            echo json_encode(FALSE);
        } else {
            echo json_encode(TRUE);
        }
    }
    // Email already check
    function email_not_registered()
    {
        if ($this->main_model->email_exists($this->input->post('email')) == TRUE) {
            echo json_encode(FALSE);
        } else {
            echo json_encode(TRUE);
        }
    }
    // Forgot password view page function
    function forgot_password_page()
    {
        if ($this->isUserLoggedIn) {
            redirect('account');
        } else {
            $data = array();
            if ($this->input->post('postForgotPassword') || $this->input->post('postResendOtp')) {
                $email = $this->input->post('email');
                $getDataEmail = $this->main_model->getData($email);
                if ($getDataEmail == $email) {
                    $otp = rand(10000, 99999);
                    $this->main_model->updateOTP($email, $otp);
                    $this->session->set_flashdata('email', $email);
                    $data['checkedEmail'] = $this->session->flashdata('email');
                    $this->session->set_flashdata('error_msg', 'OTP has been sent');
                    $data['error_msg'] = $this->session->flashdata('error_msg');
                    $this->load->view('view_enter_otp_page', $data);
                    return true;
                } else {
                    echo "Email address is not registered";
                    die;
                }
            }
            $this->load->view('view_forgotpassword');
        }
    }
    // Enter Otp function
    function enter_otp_page()
    {
        if ($this->isUserLoggedIn) {
            redirect('account');
        } else {
            $data = array();
            if ($this->input->post('postOtp')) {
                $enteredOtp = $this->input->post('otp');
                $checkedEmail = $this->input->post('email');
                $getDataPassword = $this->main_model->getDataPassword($enteredOtp, $checkedEmail);
                if ($getDataPassword != 0) {
                    $data['confirmData'] = $getDataPassword;
                    $this->session->set_userdata('success_msg', 'Otp entered is verfied please create new password !!.');
                    if ($this->session->userdata('success_msg')) {
                        $data['success_msg'] = $this->session->userdata('success_msg');
                        $this->session->unset_userdata('success_msg');
                    }
                    $this->load->view('view_new_password_page', $data);
                    return true;
                } else {
                    $data['checkedEmail'] = $checkedEmail;
                    $this->session->set_userdata('error_msg', 'Entered otp is wrong, Kindly enter correct otp !!.');
                    if ($this->session->userdata('error_msg')) {
                        $data['error_msg'] = $this->session->userdata('error_msg');
                        $this->session->unset_userdata('error_msg');
                    }
                }
            }
            $this->load->view('view_enter_otp_page', $data);
        }
    }
    // Enter new password function 
    function new_password_page()
    {
        if ($this->isUserLoggedIn) {
            redirect('account');
        } else {
            if ($this->input->post('postNewPassword')) {
                $enteredOtp = $this->input->post('otp');
                $checkedEmail = $this->input->post('email');
                $newpassword = md5($this->input->post('password'));
                $getDataPassword = $this->main_model->getDataPassword($enteredOtp, $checkedEmail);
                if($getDataPassword['email'] == $checkedEmail && $getDataPassword['otp'] == $enteredOtp) {
                    $status = $this->main_model->updatePassword($checkedEmail, $enteredOtp, $newpassword); 
                }
                redirect('main_controller');
            }
            $this->load->view('view_new_password_page');
        }
    }
}
