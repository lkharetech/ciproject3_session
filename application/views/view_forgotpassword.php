<!DOCTYPE html>
<html lang="en">

<head>
    <title>CodeIgniter User Login System by CodexWorld</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.min.js"></script>

</head>

<body>
    <div class="container">
        <br>
        <h2 style="text-align:center;">CI 3 FORGOT PASSWORD PAGE</h2>
        <hr>
        <form action="<?php echo base_url()."forgot_password_page"?>" method="POST" id="forgotPasswordForm">
            <label for="">Please enter your registerd email-id</label>
            <br>
            <input type="email" name="email" id="email" placeholder="Enter your email-id"><br><br>
            <input type="submit" id="forgotPasswordBtn" name="postForgotPassword" value="SUBMIT">
        </form>
        <hr>
        <div>
            <p>Don't have account ? <a href="<?php echo base_url('main_controller/registration'); ?>">Register</a></p>
        </div>
    </div>

    <!-- <script src="..\assets\customJS\validation.js"></script> -->
    <base href="http://localhost/Project3_Session/">
    <script src="assets\customJS\fieldValidation.js"></script>
</body>

</html>