<!DOCTYPE html>
<html lang="en">

<head>
    <title>CodeIgniter User Login System by CodexWorld</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.min.js"></script>


</head>

<body>
    <div class="container"><br>
        <h2 style="text-align:center;">CI 3 LOGIN REGISTRATION PAGE</h2>
        <h4>Create a new account</h4><br>

        <!-- Status message -->
        <?php
        if (!empty($success_msg)) {
            echo '<p class="status-msg success">' . $success_msg . '</p>';
        } elseif (!empty($error_msg)) {
            echo '<p class="status-msg error">' . $error_msg . '</p>';
        }
        ?>

        <!-- Registration form -->
        <form action="<?php echo base_url()."registration"?>" method="post" id="registrationForm">
            <div>
                <label for="name">Name :</label>
                <input type="text" id="name" name="name" placeholder="Enter your name">
            </div><br>
            <div>
                <label for="email">Email Id:</label>
                <input type="email" name="email" id="email" placeholder="Enter your email-id">
            </div><br>
            <div>
                <label for="password">Password :</label>
                <input type="password" id="password" name="password" placeholder="Enter your password">
            </div><br>
            <div>
                <label for="cnf_password">Confirm Password :</label>
                <input type="password" id="cnf_password" name="cnf_password" placeholder="Confirm your password">
            </div><br>
            <div>
                <label for="gender">Gender : </label>
                <input type="radio" id="gendermale" name="gender" value="male" checked>
                <label>Male</label>
                <input type="radio" id="genderfemale" name="gender" value="female">
                <label>Female</label>
            </div><br>
            <div>
                <label for="mobile">Mobile No. :</label>
                <input type="number" name="mobile" id="mobile" placeholder="Enter your mobile no.">
                <p>(Note : Only number can be inserted.)</p>
            </div>
            <div>
                <input type="submit" id="signupbtn" name="postSignup" value="CREATE ACCOUNT">
            </div>
        </form>
        <br>
        <p>Already have account ? <a href="<?php echo base_url('main_controller/login'); ?>">Login Here</a></p>
    </div>

    <base href="http://localhost/Project3_Session/">
    <!-- <script src="assets\customJS\validation.js"></script> -->
    <script src="assets\customJS\fieldValidation.js"></script>

</body>

</html>