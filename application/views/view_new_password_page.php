<!DOCTYPE html>
<html lang="en">

<head>
    <title>CodeIgniter User Login System by CodexWorld</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.min.js"></script>

</head>

<body>
    <div class="container">
        <br>
        <h2 style="text-align:center;">CI 3 ENTER NEW PASSWORD</h2>
        <hr>
        <!-- Status message -->
        <?php
        if (!empty($success_msg)) {
            echo '<p style="color:green;">' . $success_msg . '</p>';
        } elseif (!empty($error_msg)) {
            echo '<p style="color:red;">' . $error_msg . '</p>';
        }
        ?>
        <form action="<?php echo base_url()."new_password_page"?>" method="POST" id="newPasswordForm">
            <div>
                <input type="hidden" name="email" value="<?php echo $confirmData['email']; ?>">
                <input type="hidden" name="otp" value="<?php echo $confirmData['otp']; ?>">
                <label>ENTER NEW PASSWORD :</label><br>
                <input type="password" id="password" name="password" placeholder="Enter your password"><br>
            </div>
            <div><br>
                <label>CONFIRM PASSWORD :</label><br>
                <input type="password" id="cnf_password" name="cnf_password" placeholder="Confirm your password"><br>
            </div><br>
            <input type="submit" id="newPasswordBtn" name="postNewPassword" value="SUBMIT">
        </form>
        <br>


        <hr>
    </div>

    <base href="http://localhost/Project3_Session/">
    <!-- <script src="assets\customJS\validation.js"></script -->
    <script src="assets\customJS\fieldValidation.js"></script>

</body>

</html>