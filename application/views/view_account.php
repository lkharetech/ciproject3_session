<!DOCTYPE html>
<html lang="en">

<head>
    <title>CodeIgniter User Login System by CodexWorld</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

</head>

<body>
    <div class="container">
        <br>
        <h2 style="text-align:center;">CI 3 LOGIN ACCOUNT PAGE</h2>
        <h3>Welcome <?php echo $user['name']; ?> !!</h3>
        <hr>
        <div>
            <h6>Name : <?php echo $user['name']?></h6>
            <h6>Email-id : <?php echo $user['email']?></h6>
            <h6>Gender : <?php echo $user['gender']?></h6>
            <h6>Mobile : <?php echo $user['mobile']?></h6>
        </div>
        <a href="<?php echo base_url('main_controller/logout')?>">LOGOUT</a>
    </div>
</body>

</html>